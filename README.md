# Algorithmic Robotics #

### Contents ###
* camcal
    * **calibrate.py** - perform an intrinsic camera calibration using images of a checkerboard
    * **capture.py** - capture a still image from the camera
* teleop
    * **camera_preview.py** - display the camera's view with OpenCV (possible to view over VNC)
    * **dagudrive.py** - wrapper class for Adafruit motor drivers
    * **keyboard_control.py** - control the car using arrow keys

### Dependencies ###

* cv2

### Tested Environments ###

* Raspberry Pi 3, Ubuntu Mate 14.04, Python 2.7.12, OpenCV 2.4.9